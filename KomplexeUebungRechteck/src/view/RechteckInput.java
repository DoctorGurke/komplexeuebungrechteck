package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RechteckInput extends JFrame {

	public BunteRechteckeController inputController = new BunteRechteckeController();
	
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RechteckInput frame = new RechteckInput();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RechteckInput() {
		setTitle("Neues Rechteck erstellen");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] {100, 0, 200};
		gbl_contentPane.rowHeights = new int[] {50, 25, 25, 25, 25, 25, 25, 25, 25};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel Label_Header = new JLabel("Rechteck Eingabemaske");
		GridBagConstraints gbc_Label_Header = new GridBagConstraints();
		gbc_Label_Header.gridwidth = 3;
		gbc_Label_Header.insets = new Insets(0, 0, 5, 0);
		gbc_Label_Header.gridx = 0;
		gbc_Label_Header.gridy = 0;
		contentPane.add(Label_Header, gbc_Label_Header);
		
		/*
		 * 
		 * 
		 *  INPUT X
		 * 
		 * 
		 */
		
		//ERROR
		JLabel Label_ErrorX = new JLabel("");
		Label_ErrorX.setForeground(Color.RED);
		GridBagConstraints gbc_Label_ErrorX = new GridBagConstraints();
		gbc_Label_ErrorX.gridwidth = 2;
		gbc_Label_ErrorX.anchor = GridBagConstraints.NORTH;
		gbc_Label_ErrorX.insets = new Insets(0, 0, 5, 0);
		gbc_Label_ErrorX.gridx = 1;
		gbc_Label_ErrorX.gridy = 2;
		contentPane.add(Label_ErrorX, gbc_Label_ErrorX);
		
		//LABEL
		JLabel Label_InputX = new JLabel("X Position:");
		GridBagConstraints gbc_Label_InputX = new GridBagConstraints();
		gbc_Label_InputX.anchor = GridBagConstraints.SOUTHEAST;
		gbc_Label_InputX.insets = new Insets(0, 0, 5, 5);
		gbc_Label_InputX.gridx = 0;
		gbc_Label_InputX.gridy = 1;
		contentPane.add(Label_InputX, gbc_Label_InputX);
		
		
		//INPUT
		JTextField Text_InputX = new JTextField();
		
		Text_InputX.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent event) {
				try {
					Integer.valueOf(Text_InputX.getText());
					Label_ErrorX.setText("");
					if (Integer.parseInt(Text_InputX.getText()) > 1200) {
						Text_InputX.setText("1200");
					} /*else if (Integer.parseInt(Text_InputX.getText()) < 0) {
						Text_InputX.setText("0");
					}*/
				} catch (Exception exception) {
					if (!Text_InputX.getText().equals("")) {
						Label_ErrorX.setText("Ungültiger Datentyp!");
					} else {
						Label_ErrorX.setText("");
					}
					
				}
				
			}
		});
		
		Text_InputX.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if (Text_InputX.getText().equals("0"))
					Text_InputX.setText("");
			}
			@Override
			public void focusLost(FocusEvent e) {
				if (Text_InputX.getText().equals(""))
					Text_InputX.setText("0");
			}
		});
		
		Text_InputX.setText("0");
		Text_InputX.setToolTipText("");
		GridBagConstraints gbc_Text_InputX = new GridBagConstraints();
		gbc_Text_InputX.anchor = GridBagConstraints.SOUTH;
		gbc_Text_InputX.fill = GridBagConstraints.HORIZONTAL;
		gbc_Text_InputX.insets = new Insets(0, 0, 5, 0);
		gbc_Text_InputX.gridx = 2;
		gbc_Text_InputX.gridy = 1;
		contentPane.add(Text_InputX, gbc_Text_InputX);
		
		/*
		 * 
		 * 
		 *  INPUT Y
		 * 
		 * 
		 */
		
		
		//ERROR
		JLabel Label_ErrorY = new JLabel("");
		Label_ErrorY.setForeground(Color.RED);
		GridBagConstraints gbc_Label_ErrorY = new GridBagConstraints();
		gbc_Label_ErrorY.gridwidth = 2;
		gbc_Label_ErrorY.anchor = GridBagConstraints.NORTH;
		gbc_Label_ErrorY.insets = new Insets(0, 0, 5, 0);
		gbc_Label_ErrorY.gridx = 1;
		gbc_Label_ErrorY.gridy = 4;
		contentPane.add(Label_ErrorY, gbc_Label_ErrorY);
		
		//LABEL
		JLabel Label_InputY = new JLabel("Y Position:");
		GridBagConstraints gbc_Label_InputY = new GridBagConstraints();
		gbc_Label_InputY.anchor = GridBagConstraints.SOUTHEAST;
		gbc_Label_InputY.insets = new Insets(0, 0, 5, 5);
		gbc_Label_InputY.gridx = 0;
		gbc_Label_InputY.gridy = 3;
		contentPane.add(Label_InputY, gbc_Label_InputY);
		
		//INPUT
		JTextField Text_InputY = new JTextField();
		
		Text_InputY.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent event) {
				try {
					Integer.valueOf(Text_InputY.getText());
					Label_ErrorY.setText("");
					if (Integer.parseInt(Text_InputY.getText()) > 1200) {
						Text_InputY.setText("1200");
					} /*else if (Integer.parseInt(Text_InputX.getText()) < 0) {
						Text_InputX.setText("0");
					}*/
				} catch (Exception exception) {
					if (!Text_InputY.getText().equals("")) {
						Label_ErrorY.setText("Ungültiger Datentyp!");
					} else {
						Label_ErrorY.setText("");
					}
					
				}
				
			}
		});
		
		Text_InputY.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if (Text_InputY.getText().equals("0"))
					Text_InputY.setText("");
			}
			@Override
			public void focusLost(FocusEvent e) {
				if (Text_InputY.getText().equals(""))
					Text_InputY.setText("0");
			}
		});
		
		Text_InputY.setText("0");
		Text_InputY.setToolTipText("");
		GridBagConstraints gbc_Text_InputY = new GridBagConstraints();
		gbc_Text_InputY.anchor = GridBagConstraints.SOUTH;
		gbc_Text_InputY.fill = GridBagConstraints.HORIZONTAL;
		gbc_Text_InputY.insets = new Insets(0, 0, 5, 0);
		gbc_Text_InputY.gridx = 2;
		gbc_Text_InputY.gridy = 3;
		contentPane.add(Text_InputY, gbc_Text_InputY);
		
		/*
		 * 
		 * 
		 *  INPUT BREITE
		 * 
		 * 
		 */
		
		//ERROR
		JLabel Label_ErrorBreite = new JLabel("");
		Label_ErrorBreite.setForeground(Color.RED);
		GridBagConstraints gbc_Label_ErrorBreite = new GridBagConstraints();
		gbc_Label_ErrorBreite.gridwidth = 2;
		gbc_Label_ErrorBreite.anchor = GridBagConstraints.NORTH;
		gbc_Label_ErrorBreite.insets = new Insets(0, 0, 5, 0);
		gbc_Label_ErrorBreite.gridx = 1;
		gbc_Label_ErrorBreite.gridy = 6;
		contentPane.add(Label_ErrorBreite, gbc_Label_ErrorBreite);
		
		//LABEL
		JLabel Label_InputBreite = new JLabel("Breite:");
		GridBagConstraints gbc_Label_InputBreite = new GridBagConstraints();
		gbc_Label_InputBreite.anchor = GridBagConstraints.SOUTHEAST;
		gbc_Label_InputBreite.insets = new Insets(0, 0, 5, 5);
		gbc_Label_InputBreite.gridx = 0;
		gbc_Label_InputBreite.gridy = 5;
		contentPane.add(Label_InputBreite, gbc_Label_InputBreite);
		
		//INPUT
		JTextField Text_InputBreite = new JTextField();
		
		Text_InputBreite.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent event) {
				try {
					Integer.valueOf(Text_InputBreite.getText());
					Label_ErrorBreite.setText("");
					if (Integer.parseInt(Text_InputBreite.getText()) > 1200) {
						Text_InputBreite.setText("1200");
					} else if (Integer.parseInt(Text_InputBreite.getText()) < 0) {
						Text_InputBreite.setText(String.valueOf(Math.abs(Integer.parseInt(Text_InputBreite.getText()))));
					}
				} catch (Exception exception) {
					if (!Text_InputBreite.getText().equals("")) {
						Label_ErrorBreite.setText("Ungültiger Datentyp!");
					} else {
						Label_ErrorBreite.setText("");
					}
					
				}
				
			}
		});
		
		Text_InputBreite.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if (Text_InputBreite.getText().equals("0"))
					Text_InputBreite.setText("");
			}
			@Override
			public void focusLost(FocusEvent e) {
				if (Text_InputBreite.getText().equals(""))
					Text_InputBreite.setText("0");
			}
		});
		
		Text_InputBreite.setText("0");
		Text_InputBreite.setToolTipText("");
		GridBagConstraints gbc_Text_InputBreite = new GridBagConstraints();
		gbc_Text_InputBreite.anchor = GridBagConstraints.SOUTH;
		gbc_Text_InputBreite.fill = GridBagConstraints.HORIZONTAL;
		gbc_Text_InputBreite.insets = new Insets(0, 0, 5, 0);
		gbc_Text_InputBreite.gridx = 2;
		gbc_Text_InputBreite.gridy = 5;
		contentPane.add(Text_InputBreite, gbc_Text_InputBreite);
		
		/*
		 * 
		 * 
		 *  INPUT HOEHE
		 * 
		 * 
		 */
		
		//ERROR
		JLabel Label_ErrorHoehe = new JLabel("");
		Label_ErrorHoehe.setForeground(Color.RED);
		GridBagConstraints gbc_Label_ErrorHoehe = new GridBagConstraints();
		gbc_Label_ErrorHoehe.gridwidth = 2;
		gbc_Label_ErrorHoehe.anchor = GridBagConstraints.NORTH;
		gbc_Label_ErrorHoehe.insets = new Insets(0, 0, 5, 0);
		gbc_Label_ErrorHoehe.gridx = 1;
		gbc_Label_ErrorHoehe.gridy = 8;
		contentPane.add(Label_ErrorHoehe, gbc_Label_ErrorHoehe);
		
		//LABEL
		JLabel LabelInputHoehe = new JLabel("H\u00F6he:");
		GridBagConstraints gbc_LabelInputHoehe = new GridBagConstraints();
		gbc_LabelInputHoehe.anchor = GridBagConstraints.SOUTHEAST;
		gbc_LabelInputHoehe.insets = new Insets(0, 0, 5, 5);
		gbc_LabelInputHoehe.gridx = 0;
		gbc_LabelInputHoehe.gridy = 7;
		contentPane.add(LabelInputHoehe, gbc_LabelInputHoehe);
		
		//INPUT
		JTextField Text_InputHoehe = new JTextField();
		
		Text_InputHoehe.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent event) {
				try {
					Integer.valueOf(Text_InputHoehe.getText());
					Label_ErrorHoehe.setText("");
					if (Integer.parseInt(Text_InputHoehe.getText()) > 1200) {
						Text_InputHoehe.setText("1200");
					} else if (Integer.parseInt(Text_InputHoehe.getText()) < 0) {
						Text_InputHoehe.setText(String.valueOf(Math.abs(Integer.parseInt(Text_InputHoehe.getText()))));
					}
				} catch (Exception exception) {
					if (!Text_InputHoehe.getText().equals("")) {
						Label_ErrorHoehe.setText("Ungültiger Datentyp!");
					} else {
						Label_ErrorHoehe.setText("");
					}
					
				}
				
			}
		});
		
		Text_InputHoehe.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if (Text_InputHoehe.getText().equals("0"))
					Text_InputHoehe.setText("");
			}
			@Override
			public void focusLost(FocusEvent e) {
				if (Text_InputHoehe.getText().equals(""))
					Text_InputHoehe.setText("0");
			}
		});
		
		Text_InputHoehe.setText("0");
		Text_InputHoehe.setToolTipText("");
		GridBagConstraints gbc_Text_InputHoehe = new GridBagConstraints();
		gbc_Text_InputHoehe.anchor = GridBagConstraints.SOUTH;
		gbc_Text_InputHoehe.fill = GridBagConstraints.HORIZONTAL;
		gbc_Text_InputHoehe.insets = new Insets(0, 0, 5, 0);
		gbc_Text_InputHoehe.gridx = 2;
		gbc_Text_InputHoehe.gridy = 7;
		contentPane.add(Text_InputHoehe, gbc_Text_InputHoehe);
		
		/*
		 * 
		 * 
		 * BUTTONS
		 * 
		 * 
		 */
		
		//RANDOM
		JButton Button_Random = new JButton("Random");
		
		Button_Random.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Rechteck randRechteck = Rechteck.generiereZufallsRechteck();
				Text_InputX.setText(String.valueOf(randRechteck.getX()));
				Text_InputY.setText(String.valueOf(randRechteck.getY()));
				Text_InputBreite.setText(String.valueOf(randRechteck.getBreite()));
				Text_InputHoehe.setText(String.valueOf(randRechteck.getHoehe()));
			}
		});
		
		GridBagConstraints gbc_Button_Random = new GridBagConstraints();
		gbc_Button_Random.insets = new Insets(0, 0, 5, 0);
		gbc_Button_Random.gridwidth = 3;
		gbc_Button_Random.fill = GridBagConstraints.VERTICAL;
		gbc_Button_Random.gridx = 0;
		gbc_Button_Random.gridy = 9;
		contentPane.add(Button_Random, gbc_Button_Random);
		
		//ADD
		JButton Button_RechteckAdd = new JButton("Rechteck hinzuf\u00FCgen");
		
		Button_RechteckAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int x,y,breite,hoehe;
				if(!(Label_ErrorX.getText() == "") || !(Label_ErrorY.getText() == "") || !(Label_ErrorBreite.getText() == "") || !(Label_ErrorHoehe.getText() == "") ) {
					System.out.println("ERROR");
				} else {
					x = Integer.parseInt(Text_InputX.getText());
					y = Integer.parseInt(Text_InputY.getText());
					breite = Integer.parseInt(Text_InputBreite.getText());
					hoehe = Integer.parseInt(Text_InputHoehe.getText());
					System.out.println(x + " " + y + " " + breite + " " + hoehe);
					inputController.add(new Rechteck(x, y, breite, hoehe));
				}
				
				
			}
		});
		
		GridBagConstraints gbc_Button_RechteckAdd = new GridBagConstraints();
		gbc_Button_RechteckAdd.gridwidth = 3;
		gbc_Button_RechteckAdd.insets = new Insets(0, 0, 0, 5);
		gbc_Button_RechteckAdd.gridx = 0;
		gbc_Button_RechteckAdd.gridy = 10;
		contentPane.add(Button_RechteckAdd, gbc_Button_RechteckAdd);
	}

}
