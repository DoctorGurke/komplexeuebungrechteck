package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import controller.BunteRechteckeController;

public class Zeichenflaeche extends JPanel {
	
	private BunteRechteckeController controller;
	
	public Zeichenflaeche(BunteRechteckeController controller) {
		this.controller = controller;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		for (int i = 0; i < controller.getRechtecke().size(); i++) {
			g.drawRect(controller.getRechtecke().get(i).getX(), controller.getRechtecke().get(i).getY(), controller.getRechtecke().get(i).getBreite(), controller.getRechtecke().get(i).getHoehe());
		}
	}
	
}
