package model;

public class Punkt {
	private int x, y;
	
	public Punkt() {
		this.setX(0);
		this.setY(0);
	}
	
	public Punkt(int x, int y) {
		this.setX(x);
		this.setY(y);
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public boolean equals(Punkt p) {
		if(p.getX() == this.getX() && p.getY() == this.getY()) 
			return true;
		else
			return false;
	}
	
	@Override
	public String toString() {
		return "Punkt [x=" + this.getX() + ", y=" + this.getY() + "]";
	}
}
