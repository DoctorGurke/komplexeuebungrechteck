package model;

import java.util.Random;

public class Rechteck {
	private int breite, hoehe;
	private Punkt punkt;
	final public static int MAX_BREITE = 1200;
	final public static int MAX_HOEHE = 1000;

	public Rechteck() {
		this.punkt = new Punkt();
		this.setBreite(0);
		this.setHoehe(0);
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		this.punkt = new Punkt(x, y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}
	
	public Punkt getPunkt() {
		return this.punkt;
	}
	
	public void setPunkt(int x, int y) {
		this.setX(x);
		this.setY(y);
	}

	public int getX() {
		return this.punkt.getX();
	}

	public int getY() {
		return this.punkt.getY();
	}

	public int getBreite() {
		return breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setX(int x) {
		this.punkt.setX(x);
	}

	public void setY(int y) {
		this.punkt.setY(y);
	}

	public void setBreite(int breite) {
		this.breite = absValue(breite);
	}

	public void setHoehe(int hoehe) {
		this.hoehe = absValue(hoehe);
	}

	private int absValue(int n) {
		return Math.abs(n);
	}

	public boolean enthaelt(int x, int y) {
		if ((x >= this.punkt.getX() && x <= (this.punkt.getX() + this.getBreite()))&&(y >= this.punkt.getY() && y <= (this.punkt.getY() + this.getHoehe())))
			return true;
			else
			return false;
	}

	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(), p.getY());
	}
	
	public boolean enthaelt(Rechteck r) {
		if(this.enthaelt(r.getPunkt()) && this.enthaelt(r.getX() + r.getBreite(), r.getY() + r.getHoehe()))
			return true;
		else
			return false;
	}

	public static Rechteck generiereZufallsRechteck() {
		Random rand = new Random();
		int randX = rand.nextInt(MAX_BREITE);
		int randY = rand.nextInt(MAX_HOEHE);
		Rechteck r = new Rechteck(randX, randY, rand.nextInt(MAX_BREITE - randX), rand.nextInt(MAX_HOEHE - randY) );
		return r;
	}
	
	@Override
	public String toString() {
		return "Rechteck [x=" + this.getX() + ", y=" + this.getY() + ", breite=" + this.breite + ", hoehe=" + this.hoehe
				+ "]";
	}

}
